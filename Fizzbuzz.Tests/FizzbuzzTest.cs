﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using FizzbuzzView.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzbuzzView.Tests
{
    /// <summary>
    /// Test cases for Fizzbuzz
    /// </summary>
    [TestClass]
    public class FizzbuzzTestCases
    {
        public FizzbuzzTestCases()
        {
            //
            // TODO: Add constructor logic here
            //

        }

        [TestMethod]
        public void Fizzbuzz_Input_One()
        {
            var fizzbuzz = new FizzbuzzClass();
            var expectedOutput1 = new List<string> { "1" };

            var actualOutput1 = fizzbuzz.GetFizzbuzz(1);

            Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert.AreEqual(expectedOutput1, actualOutput1);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Two()
        {
            var fizzbuzz = new FizzbuzzClass();
            var expectedoutput2 = new List<string> { "1", "2" };

            var actualoutput2 = fizzbuzz.GetFizzbuzz(2);

            Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert.AreEqual(expectedoutput2, actualoutput2);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Fizz()
        {
            var fizzbuzz = new FizzbuzzClass();
            var expectedOutput3 = new List<string> { "1", "2", "fizz" };

            var actualOutput3 = fizzbuzz.GetFizzbuzz(3);

            Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert.AreEqual(expectedOutput3, actualOutput3);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Four()
        {
            var fizzbuzz = new FizzbuzzClass();
            var expectedOutput4 = new List<string> { "1", "2", "fizz", "4" };

            var actualOutput4 = fizzbuzz.GetFizzbuzz(4);

            Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert.AreEqual(expectedOutput4, actualOutput4);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Buzz()
        {
            var fizzbuzz = new FizzbuzzClass();
            var expectedOutput5 = new List<string> { "1", "2", "fizz", "4", "buzz" };

            var actualOutput5 = fizzbuzz.GetFizzbuzz(5);

            Microsoft.VisualStudio.TestTools.UnitTesting.CollectionAssert.AreEqual(expectedOutput5, actualOutput5);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Fizz_Buzz()
        {
            var fizzbuzz = new FizzbuzzClass();
            var inputNumber = 15;
            var expectedOutput15 = "fizz buzz";

            var actualOutput = fizzbuzz.GetFizzbuzz(inputNumber);
            var actualOutput15 = actualOutput.ElementAt(inputNumber - 1);
            Assert.AreEqual(expectedOutput15, actualOutput15);
        }

        [TestMethod]
        public void Fizzbuzz_Input_Exception_Zero()
        {
            var fizzbuzz = new FizzbuzzClass();
            var inputNumber = 0;

            Assert.ThrowsException<ArgumentException>(() => fizzbuzz.GetFizzbuzz(inputNumber));
        }

        [TestMethod]
        public void Fizzbuzz_Input_Exception_Over_Thousand()
        {
            var fizzbuzz = new FizzbuzzClass();
            var inputNumber = 1001;

            Assert.ThrowsException<ArgumentException>(() => fizzbuzz.GetFizzbuzz(inputNumber));
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

    }
}
