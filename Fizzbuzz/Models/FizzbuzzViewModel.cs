﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace FizzbuzzView.Models
{
    public class FizzbuzzViewModel
    {
        public int UserPosiion { get; set; }
        [Range(1,1000)]// Input Number between 1 and 1000 accepted
        [Required]
        public List<String> FizzbuzzNum { get; set; }
    }
}