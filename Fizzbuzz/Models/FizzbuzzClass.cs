﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzbuzzView.Models
{
    public class FizzbuzzClass
    {
        public List<String> GetFizzbuzz(int input)
        {

            if (input < 1 | input > 1000)
            {
                throw new ArgumentException("Input number is required to be between 1 and 1000");
            }

            var inputnumbers = new List<string>();

            for (var i = 1; i <= input; i++)
            {
                inputnumbers.Add(GetFizzbuzzInput(i));
            }
            return inputnumbers;
        }

        private string GetFizzbuzzInput(int i)
        {
            if ((i % 3 == 0) && (i % 5 == 0))
            {
                return "fizz buzz";
            }
            else if (i % 3 == 0)
            {
                return "fizz";
            }
            else if (i % 5 == 0)
            {
                return "buzz";
            }
            else
            {
                return i.ToString();
            }
        }
    }
}