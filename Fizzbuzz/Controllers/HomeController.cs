﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzbuzzView.Models;

namespace FizzbuzzView.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzbuzzViewModel());
        }

        [HttpPost]
        public ActionResult Fizzbuzz(FizzbuzzViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Fizzbuzz = new FizzbuzzClass();
                model.FizzbuzzNum = Fizzbuzz.GetFizzbuzz(model.UserPosiion);
                return View(model);
            }
            else
            {
                return View(model);
            }

        }
    }
}